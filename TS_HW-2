/**
 * Задание 1 - Напишите функцию сортировки чисел которая принимает на вход массив
 * строк и тип сортировки ASC или DESC, а возвращает отсортированный в соответствии
 * с типом массив
 *
 * (arr: string[], order: Order) => string[]
 *
 * Использовать: enum
 *
 * 
 */
enum Order {
    ASC='asc',
    DESC='desc'
}
function sort(arr:string[], order): string[] {
    if(order === Order.ASC) {
        return arr.sort();
    }
    if(order === Order.DESC) {
        return arr.sort().reverse();
    }
}


/**
 * Задание 2 - Напишите функцию которая генерирует пустой объект организации в зависимости
 * от типа организационной собственности ИП или ООО
 *
 * getOrganization(type) => Organization
 *
 * Организация имеет слудющие поля:
 *  - ИНН
 *  - КПП (только для ООО)
 *  - ОКПО
 *  - Наименование
 *  - Тип
 *
 *  Использовать: enum
 *
 *  
 */
interface Organization {
    inn:number;
    kpp?:number;
    okpo:number;
    name:string;
    type:OrganizationTypes;
}

enum OrganizationTypes {
    LEGAL_ENTITY='legal-entity',
    INDIVIDUAL_ENTREPRENEUR ='individual-entrepreneur'
}

function getOrganization(type:OrganizationTypes):Organization{
    if (type === OrganizationTypes.LEGAL_ENTITY){
        return {
            inn:null,
            okpo:null,
            name:null,
            type,
            kpp:null
        }

    }
    if (type === OrganizationTypes.INDIVIDUAL_ENTREPRENEUR){
        return {
            inn:null,
            okpo:null,
            name:null,
            type

        }


    }
}

/**
 * Задание 3 - Напишите тип для описания координат x и y, напишите тип прямоугольник
 * который пересекатся с типом координат и описывает ширину и длину. Опишите тоже
 * самое через интерфейсы. Почему для типов в данном случае нельзя использовать объединение?
 *
 * 
 */
type PointsT = {
    x: number;
    y: number;
}

type RectangleT = {
    width: number;
    height: number;
} & PointsT;

const figure: RectangleT = {
    width: 100,
    height: 50,
    x: 0,
    y: 0
}

interface Points {
    x: number;
    y: number;
}

interface Rectangle extends Points{
    width: number;
    height: number;
}


/**
 * Задание 4 - Напишите тип для описания данных пришедших с сервера и возможный случай ошибки.
 * Успешный:
 * {
 *    success: true,
 *    data: { firstName: 'Oleg' }
 * }
 * Произошла ошибка:
 * {
 *    success: false,
 *    errors: [{message: 'Ошибка доступа'}]
 * }
 *
 * 
 */
type User = {
    firstname: string;
}

type SuccessfulResponse = {
    success: true;
    data: User
}

type ResError = {
    message: 'Ошибка доступа'
}

type ErrorResponse = {
    success: false;
    errors: ResError[]
}

type Responses = SuccessfulResponse | ErrorResponse;


// ----=====II never и проверка на полноту=====---- //

/**
 * Задание 5 - Напишите функцию которая в зависимости от типа окружения
 * выводит одну из возможных строк: Продакшн, Тестовое окружение, Девелоп,
 * Типы окружений: prod, test, dev
 * Позаботтесь о проверки на полноту.
 *
 * Добавить типы Стейджинг и Найтли. Убедить что в TS появилась ошибка после проверки
 * на полноту из-за необработанных случаев.
 * Типы окружений: staiging, nightly
 *
 * Использовать тип пуcтого множества never
 *
 * 
 *
 */
type Stage = 'prod' | 'test' | 'dev';

function stages (stage: Stage){
    switch (stage) {
        case 'prod':
            return 'Продакшн'
        case 'test':
            return 'Тестовое окружение'
        case 'dev':
            return 'Девелоп'
        default: const never: never = stage;
    }
}


// ----=====III Интерфейсы и Обобщения=====---- //
/**
 * Задание 6 - Опишите интерфейс пользователя со следующими свойствами. Создайте подходящий объект.
 *  - firstName (строка) - имя
 *  - secondName (строка) - фамилия
 *  - middleName (строка, необязательное поле) - среднее имя
 *
 *  interface User {
 *     firstName: string;
 *     secondName: string;
 *     middleName?: string;
 * }
 *
 * const user2: User = JSON.parse('{"firstName": "a", "secondName": "a"}');
 *
 * console.log(user2.firstName, user2.secondName);
 */

/**
 * Задание 7 - Опишите интерфейс BaseEntity:
 *  - id (T) - идентификатор пользователя
 *  - addedAt (Дата) - время создания сущности
 *  - updatedAt (Дата) - время обновления сущности
 *  - addedBy (строка) - кто добавил
 *  - updatedBy (строка) - кто последний раз обновил
 *
 * Опишите интерфейс User который расширяет тип BaseEntity с типом string
 * и полями:
 * - firstName: string;
 * - lastName: string;
 *
 * Опишите интерфейс Post который расширяет тип BaseEntity с типом number
 * и полями:
 * - title: string;
 * - authorId: number;
 *
 * 
  */
interface BaseEntity1<T>{
    id: T;
    addedAt: Date;
    updatedAt: Date;
    addedBy: string;
    updatedBy: string;
}

interface User2 extends BaseEntity1<string>{
    firstName: string;
    lastName: string;
}

interface Post extends BaseEntity1<number>{
    title: string;
    authorId: number;
}


/**
 * Задание 8 - Напишите тип для описания данных пришедших с сервера и возможный случай ошибки
 * для получения одиночной сущности и массива данных.
 * Успешный:
 * {
 *    success: true,
 *    data: T
 * }
 * Произошла ошибка:
 * {
 *    success: false,
 *    errors: [{message: 'Ошибка доступа'}]
 * }
 *
 * и
 *
 * Успешный:
 * {
 *    success: true,
 *    data: T[]
 * }
 * Произошла ошибка:
 * {
 *    success: false,
 *    errors: [{message: 'Ошибка доступа'}]
 * }
 *
 * Применить обощение для типа User с полями id, firstName, lastName
 *
 * Использовать: дженерики
 *
 * 
 */
interface User3 {
    id: number,
    firstName: string,
    lastName:string
}

interface MyError{
    message: string
}

interface ServerResponseError {
    success: false,
    errors: Error[]
}

type SuccessResponce<T> = {
    success: true,
    data: T
}
type ServerResponce<T> = SuccessResponce<T> | ServerResponseError


// ----=====IV ООП=====---- //
/**
 * Задание 9 - Опишите интерфейс пользователя посложнее.
 *
 * Пользователь, как все сущности системы полученные из базы данных имеет мета-свойства базовой
 * сущности BaseEntity:
 *  - id (T) - идентификатор пользователя
 *  - addedAt (Q) - время создания сущности
 *  - updatedAt (Q) - время обновления сущности
 *  - addedBy (P) - кто добавил
 *  - updatedBy (P) - кто последний раз обновил
 *
 * Свойства интерфейса User + BaseEntity (id: string, даты - ISOString, addedBy и updatedBy типа string ) :
 *  - roles (массив типа Role) - список ролей пользователя
 *  - firstName (строка) - имя
 *  - secondName (строка) - фамилия
 *  - middleName (строка, необязательное поле) - среднее имя
 *  - isAdmin (логическое, только для чтения) - является ли пользователь администратором
 *
 * Интферфейс Role также имеет все базовые свойства интерфейса BaseEntity (id: number, даты - Date, addedBy и updatedBy типа string ) и свои:
 *  - name (строка) - наименование роли
 *
 *  
 *
 */
interface BaseEntity <T, Q, P = string>{
    id: T;
    addedAt: Q;
    updatedAt: Q;
    addedBy: P;
    updatedBy: P;
}

interface Role extends BaseEntity <number, Date>{
    name: string;
}

interface User4 extends  BaseEntity <string, string> {
    roles: Role[];
    firstName: string;
    secondName: string;
    middleName?: string;
    readonly isAdmin: boolean;
}

// ----=====V Utility Types=====---- //
